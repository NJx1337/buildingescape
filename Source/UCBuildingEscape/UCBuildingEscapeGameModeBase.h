// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UCBuildingEscapeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UCBUILDINGESCAPE_API AUCBuildingEscapeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
